import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';
import { Badge, UncontrolledDropdown, DropdownItem, DropdownMenu, DropdownToggle, Nav, NavItem } from 'reactstrap';
import PropTypes from 'prop-types';

import { AppAsideToggler, AppNavbarBrand, AppSidebarToggler, AppSidebarForm } from '@coreui/react';
import LogoBinar from '../../assets/img/logo binar.png';
import sygnet from '../../assets/img/brand/sygnet.svg'

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultHeader extends Component {
  render() {

    // eslint-disable-next-line
    const { children, ...attributes } = this.props;

    return (
      <React.Fragment>
      {/* <AppSidebarToggler className="d-lg-none" display="md" mobile /> */}
        
          <AppNavbarBrand
            full={{ src: LogoBinar, width: 89, height: 25, alt: 'Logo Binar'}}
      // minimized={{ src: sygnet, width: 30, height: 30, alt: 'CoreUI Logo' }} 
          />
          
         
       <AppSidebarForm className="d-md-down-none" display="lg"/> 

      {/*  <Nav className="d-md-down-none" navbar>
          <NavItem className="px-3">
            <NavLink to="/dashboard" className="nav-link" >Dashboard</NavLink>
          </NavItem>
          <NavItem className="px-3">
            <Link to="/users" className="nav-link">Users</Link>
          </NavItem>
          <NavItem className="px-3">
            <NavLink to="#" className="nav-link">Settings</NavLink>
          </NavItem>
    </Nav> */}
        <Nav className="ml-auto" navbar>
          <NavItem className="d-md-down-none text-white">
            <NavItem className="nav-item">Budi Handoko</NavItem>
            <NavItem className="nav-item">Candidate</NavItem>
          </NavItem>

          <button className='btn btn-square text-white' style={{
            background: '#F67280', borderRadius: '4px', paddingLeft: '35px', paddingRight: '35px', marginRight: '50px', marginLeft: '30px', paddingTop: '5px', paddingBottom: '5px'
          }}>Keluar</button>
  </Nav> 
  {/*<AppAsideToggler className="d-md-down-none" /> */} 
        {/*<AppAsideToggler className="d-lg-none" mobile />*/}
      </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

export default DefaultHeader;
